---
layout: markdown_page
title: "Business Operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

{::options parse_block_html="true" /}

## Purpose
* Facilitating systems, workflows and processes for the three functional groups - [Sales](/handbook/sales), [Marketing](/handbook/marketing) and [Customer Success](/handbook/customer-success/) - that work closely together.
* Tracking all tools, license allocation, admins and contracts for the tech stack.
* Creating processes, systems and documentation for various interdepartmental groups.
* Maintaining an accurate and up-to-date data warehouse (product) and building our analyses for stakeholders in other functions as-needed (service).
     * The result of that service may be a dashboard, which can become a new product that is maintained.
     * As a service function, the data team can often be responsible for support, answering questions such as "Where does a dashboard exist?" or "What does a metric mean?"

## Teams
* [IT-Operations](/handbook/business-ops/it-ops-team/)
* [IT Help](/handbook/business-ops/it-help/)
* [Business Systems](/handbook/business-ops/business_systems/)
* [Data](/handbook/business-ops/data-team/)
* Procurement

### Metrics
[Performance Indicators](/handbook/business-ops/metrics/)

## Reaching the Teams (internally)
- Open an issue
    - [Business Operations](https://gitlab.com/gitlab-com/business-ops/Issues.help/issues) 
    - [IT-Help](https://gitlab.com/gitlab-com/business-ops/bizops-IT-help/hd-issue-tracker/issues)
    - [IT-Ops](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/issues) 
    - [Access Requests](https://gitlab.com/gitlab-com/access-requests/issues)
    - [Laptop Requests](https://gitlab.com/gitlab-com/laptop-requests)
- Groups in GitLab
    - `@gitlab-com/business-ops/bizops-IT-help` 
    - `@gitlab-com/business-ops/bizops-bs`
    - `@gitlab-com/business-ops`
    - `@gitlab-com/business-ops/itops`
    - `@gitlab-com/business-ops/procurement`
- Channels in Slack
    - `#business-operations`
    - `#bizops-bsa`
    - `#it_help`
    - `#it-ops`

## Data Quality Process
Data isn't perfect, processes change, people make mistakes. We recognize that and make sure that when we identify an opportunity to be more correct we are moving quickly to resolution.

Our [Data Quality Process](/handbook/business-ops/data-team/data-quality-process/) is meant to capture and document these improvements as they're discovered, and to ensure that communication is made to impacted parties in a timely manner. Learn more about [Data Quality](/handbook/business-ops/data-team/data-quality).


## [Resources Documentation](/handbook/business-ops/resources)



## Labels
 
| Label                            | Description                                                                                     | project/group          | type   |
|:-------                          |:-------                                                                                         |:----------:            |:-------:|
| BusinessOPS                      | BZO is actively involved                                                                        | gitlab-com, gitlab-org | -      |
| BusinessOPS-FYI                  | No direct action needed from Bizops, but awareness for potential support/questions              | gitlab-com, gitlab-org | -      |
| BSA                              | business systems analyst work                                                                   | gitlab-com, gitlab-org | -      |
| BSS                              | business systems specialist work                                                                | gitlab-com, gitlab-org | -      |
| IT-Help                          | IT-Help work                                                                                    | gitlab-com, gitlab-org | -      |
| IT-Ops                           | IT-Ops work                                                                                     | gitlab-com, gitlab-org | -      |
| IT:: to do, IT::done             | indicates if IT needs to do work                                                                | gitlab-com             | scoped |
| waiting on license               | cannot complete AR without new license order                                                    | gitlab-com             | -      |
| BizOPS-Status::1-Needs Review    | This has not been looked at.                                                                    | gitlab-com, gitlab-org | scoped |
| BizOPS-Status::2-Backlog         | This work is in the backlog and will be pulled forward when expedient                           | gitlab-com, gitlab-org | scoped |
| BizOPS-Status::3-Working         | This work is in progress.                                                                       | gitlab-com, gitlab-org | scoped |
| BizOPS-Status::4-Change          | This needs a change in order to be approved or merged                                           | gitlab-com, gitlab-org | scoped |
| BizOPS-Status::5-Needs Info      | This needs information from requestor to move forward                                           | gitlab-com, gitlab-org | scoped |
| BizOPS-Status::6-On Hold         | This is on hold for a longer period of time                                                     | gitlab-com, gitlab-org | scoped |
| BizOPS-Status::7-Ready to Deploy | This is ready to be merged or the work executed                                                 | gitlab-com, gitlab-org | -      |
| BizOPS-Status::8-Denied          | This work is considered incomplete and won't be worked on or will not be worked on at this time | gitlab-com, gitlab-org | scoped |
| BizOPS-Priority::1               | Critical                                                                                        | gitlab-com, gitlab-org | scoped |
| BizOPS-Priority::2               | Important not urgent                                                                            | gitlab-com, gitlab-org | scoped |
| BizOPS-Priority::3               | No rush to do, but please do it.                                                                | gitlab-com, gitlab-org | scoped |
| BizOPS-Process                   | Change or addition to process/operations                                                        | gitlab-com, gitlab-org | -      |
| blocker                          | This blocks other work                                                                          | gitlab-com, gitlab-org | -      |
| laptop-request                   | laptop request                                                                                  | gitlab-com, gitlab-org | -      |


## BizOps Planning Hat
A "hat" refers to a responsibility that any team member can pick up. It is not part of a specific role or job description. A "hat" should not make up more than 10% (half of a work day) of an employee's responsibilities.

The team member who is putting on the planning hat is responsible for:
* maintaining their team's planning process up to date in the handbook- Data & IT Ops
* iterate on their team's planning process based on team member feedback
* assign issues to a milestone and team members
* document, implement, and uphold a milestone timeline that works for your team

The team member who is putting on the planning hat is *not* responsible for:
* your manager's job- the planning hat is for planning, not for managing careers; this is not a manager role or an interim manager's role
* removing blockers
* scoping issues, outside of planning

## Business Operations Team Organization

![](/handbook/business-ops/images/team1.png)

## Tech Stack
The GitLab [Tech Stack](/handbook/business-ops/tech-stack-applications/) is the approved list of applications. It lists additional information such as the Business Purpose, Who should have access, Contact/Admin, Data Classification, Okta information, and Sox Compliance information.


## What is the difference between Business Ops, Sales Ops, Marketing Ops, Marketing Programs, IT Operations, and IT Helpdesk?

| Business Operations                                                                                                                | Sales Operations                                                                                                                                                 | Marketing Operations                                                                   | Marketing Programs                                                                                                   | IT Operations                                                                                                                                                                                                | IT Helpdesk                                                                                                                             |
| :---                                                                                                                               | :---                                                                                                                                                             | :---                                                                                   | :---                                                                                                                 | :---                                                                                                                                                                                                         | :---                                                                                                                                    |
| Answering the question: “How do we build go-to-market infrastructure to support company growth and pivots?”                        | Quota assignments                                                                                                                                                | Day-to-day management, maintenance and integrations in the entire tech stack           | Day-to-day program and marketer support, including landing pages, Marketo program and Salesforce campaign management | Ensures sufficient controls and streamlines cross-fuctional & cross-system processes, such as onboarding & provisioning                                                                                      | Triage all IT related questions as they arise                                                                                           |
| Strategic analytics and data management                                                                                            | Field &amp; RD Forecasting                                                                                                                                       | New technology review, implementation, customization, documentation and administration | Development and growth of webcast program                                                                            | Build and maintain controls via monitoring, source controls, and automated processes to enable sufficient controls, without the inefficiencies of complete separation of duties via organizational hierarchy | Build a knowledge base of IT practices and pragmatic problem solving in the handbook                                                    |
| Cross-functional process automation and optimization                                                                               | Sales Compensation                                                                                                                                               | Lead management & Data enrichment                                                      | Improve and develop newsletter program                                                                               | Development and maintenance of automation of systems and processes for G&A and with Engineering to eliminate bottlenecks and slow manual activities.                                                         | Account management for password resets and lockout                                                                                      |
| New business technology review                                                                                                     | Sales/Marketing SLAs                                                                                                                                             | Grow the opt-in communication channel                                                  | Support Field Marketing with conference, event & VIP invitation, reminder and follow-up                              |                                                                                                                                                                                                              | On call support for immediate software and hardware issues during local business hours                                                  |
| Holistic business systems administration, development, integration, architecture, and planning around the entire GTM tool stack.   | Ad hoc analysis for CRO                                                                                                                                          | Marketing process design                                                               |                                                                                                                      |                                                                                                                                                                                                              | Diagnose computer errors and provide technical support                                                                                  |
| Prioritization of business operations issues                                                                                       | Sales Process Training/Documentation                                                                                                                             | Marketing & Sales process training and documentation                                   |                                                                                                                      |                                                                                                                                                                                                              | Troubleshoot software and hardware                                                                                                      |
| Prioritization and resolution of non-development customer portal process and support issues                                        | Sales Enablement functions and sales training on process flows                                                                                                   | Prioritization of Marketing-related business operations issues                         |                                                                                                                      |                                                                                                                                                                                                              | Support Weekly IT Onboarding Sessions for new Team Members                                                                              |
| Prioritization and resolution of non-development license app and signups process and support issues                                | Salesforce administration, development, architecture, customizations, and technical documentation                                                                | Marketing tech stack implementations, development and integrations                     |                                                                                                                      |                                                                                                                                                                                                              | Train end-users how to setup and use new technologies                                                                                   |
| Reporting infrastructure                                                                                                           | Evaluate, purchase and implement new technologies into GitLab tech stack                                                                                         | Marketo and Salesforce administration to support marketing activities                  |                                                                                                                      |                                                                                                                                                                                                              | Provide technical support over the phone or Web                                                                                         |
|                                                                                                                                    | Reporting and business analysis related to the sales function                                                                                                    | List pull requests                                                                     |                                                                                                                      |                                                                                                                                                                                                              | Use specialized help desk support software to take control of end-users' computers to troubleshoot, diagnose and resolve complex issues |
|                                                                                                                                    | Prioritization of Sales-related business operations issues.<br>This means consolidating all of the requests from sales into a single funnel with prioritization. |   |   |   |   |

