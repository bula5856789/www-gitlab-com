require 'spec_helper'

describe 'install page', :js do
  before do
    visit '/install'
  end

  context 'when browsing for the desired installation methods' do
    it 'shows a list of supported platforms' do
      expect(all('.method.omnibus .js-distro-tile').count).to eq(6)
      expect(all('.method.official a.tile').count).to eq(13)
      expect(all('.method.community a.tile').count).to eq(13)
    end

    it 'shows the installation instructions of a supported omnibus platform' do
      find('.distro-tile-1').click
      distro_instructions_ee = find('.distro-content-1 .js-platform-ee')

      within distro_instructions_ee do
        expect(page).to have_content('Install and configure the necessary dependencies')
      end
      expect(page.has_selector?('.distro-content-1 .js-platform-ce')).to eq(false)
    end

    it 'contains links for each omnibus supported platform' do
      page.all('.method.omnibus a.tile').each do |installation_tile|
        expect(installation_tile[:href]).to_not be_empty
      end
    end

    it 'contains links for each official installation method' do
      page.all('.method.official a.tile').each do |installation_tile|
        expect(installation_tile[:href]).to_not be_empty
      end
    end
  end
end
